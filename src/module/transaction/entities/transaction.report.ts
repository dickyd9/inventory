import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';

@Schema({ timestamps: true })
export class TransactionReport extends Document {
  @Prop({ type: mongoose.Types.ObjectId, ref: 'Transaction', index: true })
  transactionId: mongoose.Types.ObjectId;

  @Prop({ type: Date, default: null })
  deletedAt: Date;
}

export const TransactionReportSchema =
  SchemaFactory.createForClass(TransactionReport);
