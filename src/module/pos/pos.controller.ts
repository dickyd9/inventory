import { Controller, Get, Query } from '@nestjs/common';
import { PosService } from './pos.service';
import { Public } from 'src/config/database/meta';

@Controller('pos')
export class PosController {
  constructor(private readonly posService: PosService) {}

  @Get('menu')
  @Public()
  getMenu(
    @Query('categoryName') categoryName: string,
    @Query('keyword') keyword: any,
  ) {
    return this.posService.getMenu(categoryName, keyword);
  }

  @Get('customer-list')
  @Public()
  getCustomer(@Query('keyword') keyword: string) {
    return this.posService.getCustomer(keyword);
  }

  @Get('employee-list')
  @Public()
  getEmployee(@Query('keyword') keyword: string) {
    return this.posService.getEmployee(keyword);
  }

  @Get('services-category')
  @Public()
  getServicesCategory() {
    return this.posService.getCategory();
  }

  @Get('last-trx')
  @Public()
  getLastTransaction() {
    return this.posService.getLastTransaction();
  }

  @Get('item-qty')
  @Public()
  checkItemQty(@Query('itemId') itemId: string) {
    return this.posService.checkItemQty(itemId);
  }
}
