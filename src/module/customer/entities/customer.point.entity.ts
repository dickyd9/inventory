import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';

@Schema({ timestamps: true })
export class CustomerPoint extends Document {
  @Prop({ type: mongoose.Types.ObjectId, ref: 'Customer', index: true })
  customerId: mongoose.Types.ObjectId;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Transaction', index: true })
  transactionId: mongoose.Types.ObjectId;

  @Prop({ type: Number, default: null })
  spendTransaction: number;

  @Prop({ type: Number })
  pointAmount: number;

  @Prop({ type: Date })
  deletedAt: Date;
}

export const CustomerPointSchema = SchemaFactory.createForClass(CustomerPoint);
