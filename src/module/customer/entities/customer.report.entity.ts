import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';

@Schema({ timestamps: true, strict: true })
export class CustomerReport extends Document {
  @Prop({ type: mongoose.Types.ObjectId, ref: 'Customer', required: true })
  customerId: mongoose.Types.ObjectId;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Transaction', required: true })
  transactionId: mongoose.Types.ObjectId;

  @Prop({ type: Date, default: null })
  deletedAt: Date;
}

export const CustomerReportSchema =
  SchemaFactory.createForClass(CustomerReport);
