import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';

@Schema({ timestamps: true })
export class EmployeeTaskReport extends Document {
  @Prop({ type: String, index: true, default: null })
  employeeCode: string;

  @Prop({ type: mongoose.Schema.Types.ObjectId, index: true, default: null })
  transactionRef: mongoose.Schema.Types.ObjectId;

  @Prop({ type: Number })
  incomeEarn: number;

  @Prop({ type: String })
  itemCode: string;

  @Prop({ type: Date })
  deletedAt: Date;
}

export const EmployeeTaskReportSchema =
  SchemaFactory.createForClass(EmployeeTaskReport);
