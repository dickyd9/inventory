FROM node:alpine As development

WORKDIR /var/www/html/inventory/dist

COPY package*.json ./

RUN npm install --only=development

COPY . .

RUN npm run build

FROM node:alpine As production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /var/www/html/inventory/dist

COPY package*.json ./

RUN npm install --only=production

COPY . .

COPY --from=development /usr/src/app/dist ./dist

CMD npm run start:prod